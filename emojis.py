# !/usr/bin/python

from collections.abc import Callable
from mongodb import link, result_all, mongo_search as mongo, mongo_common, error
import codecs

# ↓ for combining lists
import itertools
from collections import Counter

# ↓ debug
import time
start = time.perf_counter()


class Emojis():
    def __init__(self, listed_emojis):
        self.listed_emojis = listed_emojis

    def generate_emoji(self, name: list) -> list:
        emojis = list()

        for emoji in name:
            emoji = chr(int(emoji[2:], 16))
            '''
            # deprecated
            # try:
            #     emoji = emoji.replace('+', '000')
            #     emoji, _ = codecs.unicode_escape_decode(f'\{emoji}', 'utf-8')
            # except Exception:
            #     emoji = emoji.lower().replace('000', '')
            #     emoji, _ = codecs.unicode_escape_decode(f'\{emoji}', 'utf-8')
            '''
            emojis.append(emoji)
        return emojis

    def get_matches(self, name: list, user: str = None) -> list:
        matcher = list()
        result = list()
        user_db = f'{user}_expanded_data'

        # if user's db exists and match found
        # если бд юзера создана и там есть результаты, то итерируемся по ним и находим соответствия
        if mongo_common.db_exists(collection_name=user_db):
            for element in name:
                match = mongo_common.search_emoji(name=element, collection_name=user_db, user_db=True)
                if match:
                    matcher += match
        # если соответствия есть, то добавляем их в переменную result
        if matcher:
            result = self.count_common(matcher=matcher, src=0)
            matcher = list()
        # else going to native db and search for matches

        # далее итерируемся по бд emojis 
        for element in name:
            match = mongo.search_emoji(name=element)
            # если результат есть - перезаписываем matcher
            if match:
                matcher = match
            # если нет - стираем список
            # else:
            #     matcher = list()
        # при любом раскладе трансформируем список в ['val', 'val', 'user', ['val']] для итерации внутри другой функции
        result.append(self.count_common(matcher=matcher))
        return result

    def count_common(self, matcher: list, src: int = 1) -> list:
        # src == 0 -> matches from user's db
        # src == 1 -> matches from native db
        counter = Counter(matcher)
        most_common = counter.most_common()
        max_count = most_common[0][1] if most_common else None

        result = list()

        for elem, count in most_common:
            if count == max_count:
                result.append(elem)
        if src:
            return result
        if result:
            result.append('from_user')
        return result

    @property
    def get_list_emojis(self):
        return result_all

# list_of_names = mongo.get_values_from_column(col_name='Name')
# list_of_desc = mongo.get_values_from_column(col_name='Description')
# list_of_values = list(itertools.chain.from_iterable(zip(list_of_names, list_of_desc)))

emojis = Emojis(listed_emojis=result_all)
