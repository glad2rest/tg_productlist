# !/usr/bin/python

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import pandas as pd
from mongodb import error, link, mongo_search
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class ParseAndFetch:
    def __init__(self, driver):
        self.driver = driver

    def connect(self, link):
        self.driver.get(link)
        table = WebDriverWait(self.driver, 20).until(
            EC.visibility_of_all_elements_located(
                (
                    By.XPATH, 
                    "//tr[th/a[contains(text(), 'food')]]/following-sibling::tr[th/a[contains(text(), 'dishware')]]/preceding-sibling::tr[preceding-sibling::tr[th/a[contains(text(), 'food')]]]"
                )
            )
        )
        return table
    
    def parse(self, link):
        list_of_ids = list()
        list_of_codes = list()
        list_of_names = list()
        list_of_description = list()

        for row in self.connect(link):
            codes = row.find_elements(By.XPATH, './/td[2]')
            names = row.find_elements(By.XPATH, './/td[4]')
            description = row.find_elements(By.XPATH, './/td[5]')

            for code, name, description in zip(codes, names, description):
                list_of_codes.append(code.text)
                list_of_names.append(name.text)
                list_of_description.append(description.text)
        
        # ↓ transforms the list of strings ['description1', 'description2', ...] 
        # into the dict of the following format {'element_seq_eng': 'description1', 'element_seq_ru': 'description2'}
        list_of_description = [{f'_eng' : ''.join(element.replace('| ', '')), f'_ru': ''} for element in list_of_description]

        list_of_ids = [id for id in range(1, len(list_of_names) + 1)]
        return list_of_ids, list_of_codes, list_of_names, list_of_description
    
    def pd_dataframe(self, id, code, name, desc, dict=0):
        df = pd.DataFrame(
            {
            '_id': id,
            'Unicode': code,
            'Name': name,
            'Description': desc,
            }
        )
        return df if dict == 0 else df.to_dict('records')

    def save_to_csv(self, dataframe):
        dataframe.to_csv('emojis.csv')


if __name__ == '__main__':
    start = time.perf_counter()

    driver = ParseAndFetch(webdriver.Chrome(service=Service(ChromeDriverManager().install())))
    id_list, code, name, desc = driver.parse('https://www.unicode.org/emoji/charts/emoji-list.html')

    mongo_search.collection = 'emojis'

    last_id_in_db = mongo_search.get_last_id()
    
    if last_id_in_db in id_list or last_id_in_db > id_list[-1]:
        id_list = list(map(lambda id_new: id_new + last_id_in_db, id_list))
    
    data = driver.pd_dataframe(id=id_list, code=code, name=name, desc=desc, dict=1)
    
    try:
        mongo_search.add_rows(data)
    except error:
        print(error.details, '*')
    
    
    end = time.perf_counter()

