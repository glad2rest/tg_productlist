import sys
sys.path.insert(0, '/data/Telegram/tg_buylist')
import telebot

import pymongo
import os
import pytest
from dotenv import load_dotenv
from mongodb import MongoDB

@pytest.fixture(scope='session', autouse=True)
def dotenv():
    load_dotenv('.env')

class TestCaseTelegramMock:
    @pytest.fixture(scope='class', autouse=True)
    def foodbot(self):
        bot = telebot.TeleBot(os.getenv('token', None))
        return bot
    
    def test_connection(self, foodbot):
        bot = foodbot
        assert bot.get_me().id != 0

    def test_bot_message(self, foodbot):
        chat_id = os.getenv('chat_id')
        bot_msg = foodbot.send_message(chat_id=chat_id, text='/start')
        assert bot_msg.text == '/start'

    def test_new_id_for_stickers(self, foodbot):
        chat_id = os.getenv('chat_id')
        sticker = 'CAACAgIAAxkBAAJCJWRknmzRj-o7JULwHKZBvTq6RbQkAAIfAAPANk8T5Dgz94RSmZIvBA'
        bot_msg = foodbot.send_sticker(chat_id=chat_id, sticker=sticker)
        assert bot_msg.sticker.file_id != 'CAACAgIAAxkBAAJCJWRknmzRj-o7JULwHKZBvTq6RbQkAAIfAAPANk8T5Dgz94RSmZIvBA'

    def test_keyboard(self, foodbot):
        chat_id = os.getenv('chat_id')
        buttons = [
            telebot.types.InlineKeyboardButton(text='test', callback_data='test_call'),
            telebot.types.InlineKeyboardButton(text='test2', callback_data='test2_call'),
        ]
        keyboard = telebot.types.InlineKeyboardMarkup().add(*buttons)
        foodbot.send_message(chat_id=chat_id, text='Buttons here', reply_markup=keyboard)
        for buttons in keyboard.keyboard:
            for number, button in enumerate(buttons):
                assert button.text == ['test', 'test2'][number]
                assert button.callback_data == ['test_call', 'test2_call'][number]

    def test_create_list(self, foodbot):
        chat_id = user_id = os.getenv('chat_id')
        foodbot.send_message(chat_id=chat_id, text='Pizza')
        session = {
            user_id: {
                'food_list': [],
            }
        }
        def add_to_session(chat_id, object):
            session[chat_id]['food_list'].append(object)
            return session[chat_id]['food_list']
        
        @foodbot.message_handler(content_types=['text'])
        def get_message(message):
            if message.text == 'Pizza':
                add_to_session(chat_id=chat_id, object='Pizza')
                session[user_id].update({'msg': message.text})
                foodbot.change_message_text(chat_id=chat_id, message_id=message.id, text='azziP')

        food_list = add_to_session(chat_id, 'Pizza')
        assert len(food_list) == 1
        assert food_list[0] == 'Pizza'

class TestCaseMongoDBMock:
    @pytest.fixture(scope='class', autouse=True)
    def connect(self):
        link = os.getenv('mongodb_uri')
        connect = pymongo.MongoClient(link)
        yield connect
        connect.close()

    @pytest.fixture(scope='class', autouse=True)
    def collection(self, connect, request):
        cluster_name = 'test2'
        cluster = connect[cluster_name]
        collection = cluster['test_collection']

        def finally_drop():
            collection.drop()

        request.addfinalizer(finally_drop)  # register function execution at the end of tests
        yield collection

    def test_name_collection(self, collection):
        collection_name = 'test_and_delete'
        check = collection[collection_name]
        assert check.name == 'test_collection.test_and_delete'

    def test_create_new_and_delete(self, connect):
        db_name = 'db_test_name'
        assert db_name not in connect.list_database_names()

    def test_add_row(self, collection):
        collection.insert_one({'name': 'John Test'})
        result = collection.find_one({'name': 'John Test'})
        assert result is not None
        assert result['name'] == 'John Test'

    def test_add_rows(self, collection):
        names = ('Jason', 'Jessy', 'Jil')
        age = (23, 31, 41)
        create_date = ('2020.03.11', '2021.04.11', '2022.05.11')
        data = list()
        for pk, index in enumerate(range(len(names)), start=10):
            data.append(
                {
                    '_id': pk,
                    'name': names[index],
                    'age': age[index],
                    'date_create': create_date[index],
                },
            )
        collection.insert_many(data)
        results = [row for row in collection.find({'_id': {'$in': [10, 11, 12]}})]
        expected = [
            {
                '_id': '10',
                'name': 'Jason',
                'age': 23,
                'date_create': '2020.03.11',
            },
            {
                '_id': '11',
                'name': 'Jessy',
                'age': 31,
                'date_create': '2021.04.11',
            },
            {
                '_id': '12',
                'name': 'Jil',
                'age': 41,
                'date_create': '2022.05.11',
            },
        ]
        
        assert len(results) == 3
        for expect in expected:
            for result in results:
                if expect['_id'] == result['_id']:
                    assert result['name'] == expect['name']
                    assert result['age'] == expect['age']
                    assert result['date_create'] == expect['date_create']

        assert results[0]['name'] == 'Jason'
        assert results[1]['age'] == 31
        assert results[2]['date_create'] == '2022.05.11'

    def test_check_last_id(self, collection):
        collection.insert_one({'_id': 4, 'name': 'Check my id'})
        check_id = collection.find({'name': 'Check my id'})[0]['_id']
        assert check_id == 4

    def test_count_tmp_documents(self, collection):
        check_count = collection.count_documents({})
        assert check_count == 5
    
    ### methods that needed for the real db tests ###

    def check_collection(self, collection_name):
        collection_name = 'emojis'
        return collection_name
    
    def specials_slayer(*args, **kwargs):
        return MongoDB.specials_slayer(*args, **kwargs)
    
    def db_exists(*args, **kwargs):
        return MongoDB.db_exists(*args, **kwargs)
    
    def add_collection(*args, **kwargs):
        return MongoDB.add_collection(*args, **kwargs)

    def get_last_id(*args, **kwargs):
        return MongoDB.get_last_id(*args, **kwargs)
    
    def add_row(*args, **kwargs):
        return MongoDB.add_row(*args, **kwargs)
    
    ###
    
    def test_count_real_documents(self, connect):
        self.db = connect['tg_awkward']
        check_count = MongoDB.count_documents(self, collection_name='emojis', query='all')
        assert check_count == 126

    def test_special_slayer(self, *args, **kwargs):
        test_name = '#!@#t$#@e%$#s@*#t'
        check_name = MongoDB.specials_slayer(self, test_name)
        assert check_name == 'test'

    def test_searching_for_emoji(self, connect):
        self.db = connect['tg_awkward']
        check_emoji_utf = MongoDB.search_emoji(self, name='grapes', res=MongoDB.show_results(self, collection_name='emojis'))
        assert check_emoji_utf == ['U+1F347']

    def test_show_results(self, connect):
        self.db = connect['tg_awkward']
        result = MongoDB.show_results(self, collection_name='emojis', limit=1, column_name='Name')
        assert result[0]['Name'] == 'grapes'

    def test_collection_exists(self, connect):
        self.db = connect['test2']
        boolean = MongoDB.db_exists(self, collection_name='test_collection')
        assert boolean is True
