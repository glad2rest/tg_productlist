import re

def message(food, mongo_common, data, message, user_id):
    food_list = data['food_list']

    msg = food_list[-1]  # <- '1. 🙂 text'
    msg = replace_or_cut_emotion(msg, message.text, mongo_common)  # <-

    emoji = mongo_common.specials_slayer(name=message.text)

    pattern = r'\w+|\s'
    if re.match(pattern, emoji):
        emoji = re.sub(pattern, '', emoji)

    if not food.session[user_id].get('mark_expand_db', False):
        food.session[user_id]['mark_expand_db'] = expand_db = {}
    else:
        expand_db = food.session[user_id]['mark_expand_db']
    _, _, *exp_msg = msg.split()
    exp_msg = ' '.join(exp_msg)
    
    # prevents key-duplicates in session, e.g.:
    # 1. user decides to add a smile matched to his text -> session['mark_expand_db'] = {'🙂': 'text'}
    # 2. user changed his decision and added a new smile -> session['mark_expand_db'] = {'🤔': 'text'}
    if exp_msg in expand_db.values() and emoji:           
        # 3. unpacking list comrehension which contains the key of the dict 'expand_db'
        # ? need smth to add
        expand_db[emoji] = expand_db.pop(*[key for key in expand_db.keys()])
    elif exp_msg and emoji:
        expand_db.update(
            {
                emoji: exp_msg,
            }
        )
    
    food_list[-1] = msg

    match food.session[user_id]['message_id']:  # TODO: need to think how to get rid out of this
        case list():
            message_one, message_two = food.session[user_id]['message_id']
        case _:
            message_one = food.session[user_id]['message_id']
            message_two = None

    text = f'Добавлен: {food_list[-1]}'

    food.bot.edit_message_text(
        chat_id=user_id, 
        message_id=message_one, 
        text=text, 
        reply_markup=food.keyboard_create_list
    )
    food.bot.delete_message(chat_id=user_id, message_id=message.id)

    if message_two:
        # delete user's choice of emoji/text typed in the chat
        food.bot.delete_message(chat_id=user_id, message_id=message_two)

    food.session[user_id]['current_handler'] = 'create_list_handler'

def replace_or_cut_emotion(text: str, replacement: str, mongo_common: 'MongoDB'):
    emoji_position = int()
    # iterate through the string to detect only unicoded emoji
    for num, symbol in enumerate(text):
        # max iters is five because: 
        # the string may be '1. 🫣 text' -> the emoji's index is 3, caught it
        #                    ↑↑↑ ↑↑
        #                    012 34
        # or may be '1. text' -> so no need to iterate further
        #            ↑↑↑↑↑
        #            01234
        if num == 5:
            break
        if ord(symbol) > 127 and not symbol.isalpha() and not symbol.isdigit():
            emoji_position = num
            break
    
    # cut from the string all special characters
    replacement = mongo_common.specials_slayer(name=replacement)

    # implement the pattern to make sure that replacement-string is an emoji, not text/nums
    pattern = r'\w+|\s'  # all letters(UPPER/lower), numbers and space character
    match = re.match(pattern, replacement)
    if match:  # if so - erase 'replacement'
        replacement = re.sub(pattern, '', replacement)

    if replacement:                     # if replacement is the unicoded emoji
        if emoji_position:
            text = text.split()         # ['1.', '🫣', 'text']
            text.pop(1)                 # ['1.', 'text']
            text.insert(1, replacement) # ['1.', '🤔', 'text']
            text = ' '.join(text)       # '1. 🤔 text'
        else:
            text = text.split()         # ['1.', 'text']
            text.insert(1, replacement) # ['1.', '🤔', 'text']
            text = ' '.join(text)       # '1. 🤔 text'
    return text
    