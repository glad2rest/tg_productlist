import re
from typing import Union


# main keyboard builder
def keyboard_create(kb, mongo_common, user_id):
    collection_name = f'{user_id}_expanded_data'

    button_create_list = kb.button_create(text='\N{scroll} Создать новый список', callback='create_list_call')
    
    button_show_lists = kb.button_create(text='\N{bento box} Посмотреть сохранённые списки', callback='show_lists_call')
    button_erase_users_emojis_db = None

    if mongo_common.count_documents(collection_name=collection_name, query='all'):
        button_erase_users_emojis_db = kb.button_create(text='\N{wastebasket} Очистить привязку эмоций', callback='erase_udb_call')

    if button_erase_users_emojis_db:
        buttons = [button_create_list, button_show_lists, button_erase_users_emojis_db]
    else:
        buttons = [button_create_list, button_show_lists]

    keyboard_start = kb.keyboard_builder(buttons=buttons)
    return keyboard_start

def message(food, message, user_id):
    sticker_id = 'CAACAgIAAxkBAAIv4mRSItU5f-yWHYGoy6bYaCx1TLOMAAIIAAPANk8Tb2wmC94am2kvBA'
    food.bot.send_sticker(chat_id=user_id, sticker=sticker_id)

    text = f'''
Добро пожаловать в бот *"ПокупAI"*.

Я ваш личный помощник для создания списков покупок.
Я могу помочь Вам не только составить список, но и отслеживать его выполнение.
Вы можете редактировать текст записей и привязывать к ним эмоции, чтобы сделать их более выразительными.
Я также сохраню все ваши списки в безопасности и удалю их, если вы захотите.

Уверен, что мы сможем сделать Ваши покупки более удобными и приятными!
'''

    # tip: all dots should be escaped with \ character
    pattern = r'[!|.|#]'
    
    # \\\g<0> - pattern in regexp that replaces all characters in the pattern with the extra backslah
    # e.g. 'string.' -> 'string\.', 'string!' -> 'string\!'
    # "parse_mode" arg allows to format the text in telegram (bald, italic, etc.)
    text = re.sub(pattern, r'\\\g<0>', text)  # replace all characters that match the pattern (dots and exclamations)
    food.bot.send_message(chat_id=message.from_user.id, text=text, reply_markup=food.keyboard_start, parse_mode='MarkdownV2')
