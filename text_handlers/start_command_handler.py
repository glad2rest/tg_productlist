from telebot.types import ReplyKeyboardRemove
import time

def auth(food, message):
    user_id = message.from_user.id
    if user_id not in food.session:
        food.update_session(message=message)
    data = food.session[user_id]
    data['current_handler'] = 'start_handler'
    
    # silly trick: loading simulation
    if not data.get('already_welcomed'):
        remove_markup = ReplyKeyboardRemove()
        remove_markup_message = food.bot.send_message(chat_id=user_id, text='...', reply_markup=remove_markup)
        food.bot.delete_message(chat_id=user_id, message_id=remove_markup_message.message_id)
        
        sequence = ['|', '/', '-', '\\']
        msg = food.bot.send_message(chat_id=user_id, text=sequence[-1])
        for number in range(15):
            text = sequence[number % len(sequence)]
            if number == max(range(15)):
                text = 'Соединение установлено.'
            msg = food.bot.edit_message_text(chat_id=user_id, message_id=msg.message_id, text=text)
            time.sleep(0.01)
        time.sleep(1)
        food.bot.delete_message(chat_id=user_id, message_id=msg.id)
    
