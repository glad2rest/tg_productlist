from datetime import datetime
import time

def save_list(food, kb, mongo_common, message, data, user_id):
    # declare the name of list to save
    if message.text.isdigit():
        data['list_name'] = str(datetime.now().strftime('%Y.%m.%d %H:%M:%S'))
    else:
        data['list_name'] = message.text
    
    # check if the user has linked emojis to certain elements of text
    expanded_db = data['mark_expand_db'] if data.get('mark_expand_db') else None
        
    if expanded_db:
        def nested_keyboard_yes_no():
            buttons = list()
            buttons.append(kb.button_create(text='Сохранить мои эмоции', callback='yes_expand'))
            buttons.append(kb.button_create(text='Не сохранять', callback='no_expand'))
            keyboard_list = kb.keyboard_builder(buttons, width=2)
            return keyboard_list
        
        text = f'Вы привязали новые эмоции к некоторым позициям. Количество привязок: {len(expanded_db)}.\
            \nСохранить изменения в базе данных, чтобы в следующий раз иметь возможность использовать их?'
        
        last_message = food.bot.send_message(
            chat_id=user_id, 
            text=text, 
            reply_markup=nested_keyboard_yes_no()
        )
        data['message_id'] = last_message.message_id

    else:
        message = food.bot.send_message(chat_id=user_id, text='Секундочку, сохраняю.')
        text = f'Список "{data["list_name"]}" сохранен успешно.'
        food.bot.send_chat_action(chat_id=user_id, action='typing', message_thread_id=user_id, timeout=1)
        mongo_common.save_users_collection_docs(collection_name=str(user_id), data=data)
        
        food.bot.send_message(chat_id=user_id, text=text)
        time.sleep(1)
        # reset [food_list]
        food.create_list(message=message, reset=True)
        food.bot.delete_message(chat_id=user_id, message_id=message.message_id)

        food.bot.send_message(chat_id=user_id, text='Hello', reply_markup=food.keyboard_start)

        data['current_handler'] = 'start_handler'