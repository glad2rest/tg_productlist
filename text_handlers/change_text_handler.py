import re

def editor(food, data, message, user_id):
    # implement prefix in the callback.handler == "change_text_call"
    prefix = data['changing_text_prefix']
    suffix = message.text

    # implement text and update the last element in the "food_list"
    text = data['food_list'][-1] = f'{prefix} {suffix}'

    food.bot.delete_message(chat_id=user_id, message_id=message.message_id)

    # this pattern prevents the user to type emojis
    pattern = r'\w+|\s'
    if re.search(pattern, text):
        food.bot.edit_message_text(chat_id=user_id, message_id=data['message_id'], text=f'Добавлен: {text}', reply_markup=food.keyboard_create_list)
        
        if data.get('original_text'):
            del data['original_text']
        
        # the "key" is an emoji defined to the current text
        # so after changing text we'll change the data[key] value
        if data.get('mark_expand_db'):
            key = list(data['mark_expand_db'])[-1]
            data['mark_expand_db'][key] = suffix
    else:
        food.bot.edit_message_text(chat_id=user_id, message_id=data['message_id'], text=data['original_text'], reply_markup=food.keyboard_create_list)
    
    data['current_handler'] = 'create_list_handler'