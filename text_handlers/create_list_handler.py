def keyboard_create(kb):
    btn1 = kb.button_create('\N{memo} изменить текст', 'change_text_call')
    btn2 = kb.button_create('\N{nerd face} посмотреть список', 'view_list_call')
    btn3 = kb.button_create('\N{hot pepper} изменить эмоцию', 'change_emoji')
    btn4 = kb.button_create('\N{floppy disk} сохранить список', 'save_list_call')
    btn5 = kb.button_create('назад', 'back_to_the_main_call')
    buttons = [btn1, btn2, btn3, btn4, btn5]
    create_list_keyboard = kb.keyboard_builder(buttons=buttons, width=2)
    return create_list_keyboard

def update_session_with_matched_emojis(food, message, user_id):
    session = food.session[user_id]['matched_emojis'] = dict()
    matches = food.create_list(message)
    if matches:
        for emoji in matches:
            food.session[user_id]['matched_emojis'].update(
                {
                    f'emo_{len(session)}': emoji
                }
            )

def message(food, message, user_id):
    current_index = food.session[user_id]['counter'] - 1
    current_element = food.session[user_id]['food_list'][current_index]

    sent_message = food.bot.send_message(chat_id=message.from_user.id, text=f'Добавлен: {current_element}', reply_markup=food.keyboard_create_list)
    if not food.session[user_id].get('message_id'): 
        food.session[user_id]['message_id'] = sent_message.message_id

    match food.session[user_id]['message_id']:
        case list():
            message_id, _ = food.session[user_id]['message_id']
        case _:
            message_id = food.session[user_id]['message_id']

    if message_id < sent_message.message_id:
        food.bot.edit_message_reply_markup(chat_id=message.chat.id, message_id=message_id, reply_markup=None)
        food.session[user_id]['message_id'] = sent_message.message_id

def warning(food, message, max, user_id):
    emoji = '\N{face with monocle}'
    current_length = len(message.text)
    text = f'{emoji} Максимальная длина предложения не может превышать {max} символов.\
            \nУ вас - {current_length}. Сократите, пожалуйста, ввод.'
    food.bot.send_message(chat_id=user_id, text=text)
