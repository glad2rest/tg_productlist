def message(food, data, message, user_id):
    text = f'Добавлен: {data["food_list"][-1]}'

    current_message = food.bot.edit_message_text(
        chat_id=user_id, 
        message_id=data['message_id'], 
        text=text, 
        reply_markup=food.keyboard_create_list
    )
    
    food.bot.delete_message(chat_id=user_id, message_id=message.message_id)
    data['message_id'] = current_message.message_id
    data['current_handler'] = 'create_list_handler'