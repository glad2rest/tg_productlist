import pymongo
from pymongo.errors import BulkWriteError
from settings import data
import re
from time import perf_counter
from datetime import datetime
import json
import os
from tqdm import tqdm
import translators as ts

class MongoDB:
    def __init__(self, link):
        self.link = link
        self._collection_name = None
        self.client = pymongo.MongoClient(link)
        self.db = self.client[data['dbname']]

    @property
    def collection(self):
        if self._collection_name is not None:
            return self._collection_name

    @collection.setter
    def collection(self, collection_name):
        self._collection_name = collection_name

    def connect(self):  # passive
        self.db[self.collection_name]

    def close_connection(self):
        self.client.close()

    def check_collection(self, collection_name=None):  # checks if copy of the Class gets attribute (collection_name)
        if self._collection_name is not None and collection_name is None:
            collection_name = str(self.collection)
        return collection_name
        
    def db_exists(self, collection_name):
        if collection_name in self.db.list_collection_names(): # and self.count_documents(collection_name=collection_name, query='all'):
            return True
        return False

    def add_row(self, data: dict, collection_name=None):
        if self._collection_name is not None and collection_name is None:
            collection_name = str(self.collection)
        self.db[self._collection_name].insert_one(data)

    def add_rows(self, data: dict, collection_name=None):
        if self._collection_name is not None and collection_name is None:
            collection_name = str(self.collection)
        self.db[self._collection_name].insert_many(data)

    def save_users_collection_docs(self, data: dict, expand: bool = False, collection_name: str = ''):
        if expand:
            self.collection = collection_name
            if not self.db_exists(collection_name=collection_name):
                self.add_collection(collection_name=collection_name, unlimited=True)
            for key, value in data.items():
                _id = self.get_last_id(collection_name=collection_name) + 1
                document = {
                    '_id': _id,
                    'text': value.lower(),
                    'user_id': str(collection_name.split('_')[0]),
                    'emoji': key,
                }
                self.add_row(data=document)
        else:
            current_time = str(datetime.now().strftime('%Y.%m.%d %H:%M:%S'))
            self.collection = collection_name
            if not self.db_exists(collection_name=collection_name):
                self.add_collection(collection_name=collection_name)
            _id = self.get_last_id(collection_name=collection_name) + 1
            document = {
                    '_id': _id,
                    'time': current_time,
                    'name': data['first_name'],
                    'list_name': data['list_name'],
                    'food_list': data['food_list'],
                }
            self.add_row(data=document)

    def add_collection(self, collection_name: str, unlimited=False):
        if unlimited:
            self.db.create_collection(name=collection_name)
        else:
            # max - the arg that sets the amount of documents can be created in the collection
            capped: bool = True  # turns the capped_collection_mode ON
            size: int = 10000  # bytes
            max: int = 20  # allowing amount of documents in collection
            self.db.create_collection(name=collection_name, capped=capped, size=size, max=max)
    
    def truncate_collection(self, collection_name=None):
        collection_name = self.check_collection(collection_name)
        self.db[collection_name].delete_many({})

    def del_document(self, collection_name=None, *, key, value):
        collection_name = self.check_collection(collection_name)
        try:
            self.db[collection_name].delete_one({key: value})
        except BulkWriteError as err:
            return f'failed to delete key: {key} with value: {value}. Error: {err.details}'

    def drop_collection(self, collection_name=None):
        collection_name = self.check_collection(collection_name)
        question = str(input(f'Type {collection_name} to delete this Collection: '))
        if question == collection_name:
            self.db[collection_name].drop()

    def get_last_id(self, collection_name=None):
        collection_name = self.check_collection(collection_name)  # checks if argument collection_name is defined
        try:
            return self.db[str(collection_name)].find().sort('_id', -1).limit(1)[0]['_id']
        except IndexError:
            self.db_exists(collection_name)
            return 1
        
    def show_results(self, collection_name=None, sort=('_id',  1), limit=0, key='', value='', column_name='', pretty=0, raw=0) -> list:
        collection_name = self.check_collection(collection_name)
        find_case = {}

        if key and value:
            column_name = ''
            find_case = {key: value}
        elif column_name:
            key = ''
            value = ''
            find_case = {}, {column_name: 1}
        
        if raw == 1:
            result = self.db[collection_name].find(find_case).sort(*sort).limit(limit)
            return result
        else:
            if column_name:
                query_result = self.db[collection_name].find(*find_case).sort(*sort).limit(limit)
            else:
                query_result = self.db[collection_name].find(find_case).sort(*sort).limit(limit)
            result = [row for row in query_result]

            if pretty == 1:
                result = json.dumps(result, sort_keys=True, indent=4)
            if len(result) > 0:
                return result
        return result
    
    def count_documents(self, collection_name=None, *, query: any):
        collection_name = self.check_collection(collection_name=collection_name)
        if query == 'all':
            return self.db[collection_name].count_documents({})

    def specials_slayer(self, name: str) -> str:
        specials = re.escape(r'[!@#$%^&*();.,/\\]') + r'|[\[_\]\+\-=!@#$%^&*();.,/\\\[\]]'
        name = re.sub(specials, '', name).strip()

        return name
    
    def search_emoji(self, name: str, collection_name=None, user_db=False, tests=False, res=None) -> list:
        '''
        Working Monster-comprehension
        return [line.get('Unicode') for line in result_all 
            if re.search(r'\b{}\b'.format(re.sub(re.escape(r'[!@#$%^&*();.,/\\]') + r'|[!@#$%^&*();.,/\\]', '', name).strip() if re.sub(re.escape(r'[!@#$%^&*();.,/\\]') + r'|[!@#$%^&*();.,/\\]', '', name).strip() else '^$'), line['Name'])
            or re.search(r'\b{}\b'.format(re.sub(re.escape(r'[!@#$%^&*();.,/\\]') + r'|[!@#$%^&*();.,/\\]', '', name).strip() if re.sub(re.escape(r'[!@#$%^&*();.,/\\]') + r'|[!@#$%^&*();.,/\\]', '', name).strip() else '^$'), line['Description']['_eng'])]
        '''
        name = self.specials_slayer(name=name)
        slice_name = 3 if len(name) <= 6 else 4

        pattern = re.compile(rf'\b{name}|\b{name[:slice_name]}')
        if tests:
            name = name
        else:
            name = rf'\b{name}\b' if name else r'^$'
            

        if user_db:
            self.collection = collection_name
            user_id = collection_name.split('_')[0]
            result = [
                line.get('emoji') 
                for collection in users_dbs_lst
                    for line in collection
                    if re.search(name, line['text']) and line['user_id'] == user_id]
            
            return result
        
        result = [
            line.get('Unicode')
            for line in result_all 
                for lists in (
                    line['Description']['_eng'].split(), 
                    line['Name'].split(), 
                    line['Description']['_ru'].split()
                )
                    for word in lists
                        if re.fullmatch(name, word)
        ]

        if result:
            return result

        else:
            result = [
            line.get('Unicode') 
                for line in result_all 
                    if re.search(pattern, line['Name']) 
                    or re.search(pattern, line['Description']['_eng']) 
                    or re.search(pattern, line['Description']['_ru'])
            ]
            return result

    
    def search_emoji2(self, name: str, collection_name=None):
        collection_name = self.check_collection(collection_name)
        result = list()
        name = self.specials_slayer(name=name)
        name_emoji = rf'\b{name}\b' if name else r'^$'  # https://docs.python.org/3/library/re.html

        for line in result_all:
            center_word = len(name_emoji) // 2 + 1
            pattern = re.compile(name_emoji[:center_word])
            parent_dict = line['Name']
            child_dict_en = line['Description']['_eng']
            child_dict_ru = line['Description']['_ru']

            if re.search(pattern, parent_dict) or re.search(pattern, child_dict_en) or re.search(pattern, child_dict_ru):
                result.append(line.get('Unicode'))

        return result if result else None

    # unnessesary
    def search_through_columns(self, collection_name=None, data=list(), name=str()) -> bool:
        collection_name = self.check_collection(collection_name=collection_name)

        for _ in data:
            if name in '|'.join(data):
                return True
        return False

    def update_documents(self, collection_name=None, query='', update='', many=0):
        collection_name = self.check_collection(collection_name=collection_name)

        if query and update:
            if many == 1:
                self.db[collection_name].update_many(query, update)
            else:
                self.db[collection_name].update_one(query, update)

    def update_users_collection(self, collection_name=None):
        collection_name = self.check_collection(collection_name=collection_name)
        
        users_dbs = mongo_common.db.list_collection_names(filter=users_filter)
        users_dbs_lst = [mongo_common.show_results(collection_name=user) for user in users_dbs if user]
        if users_dbs_lst:
            user = collection_name.split('_')[0]
            count = -1
            for num, users in enumerate(users_dbs_lst):
                if count == -1:
                    for db in users:
                        if user in db['user_id']:
                            count = num
                            break
            users_dbs_lst[count] = self.show_results(collection_name=collection_name)

    def translate_and_put(self):
        col_name = mongo.show_results(column_name='Description')
        for row in tqdm(col_name):
            translated_result = str()
            if not row['Description']['_ru']:
                for word in row['Description']['_eng'].split():
                    translated_word = ts.translate_text(
                        from_language='en',
                        to_language='ru',
                        translator='google',
                        query_text=word
                    )
                    translated_result = f'{translated_result} {translated_word}'.strip()

                mongo.update_documents(
                    query={'_id': row['_id']},
                    update={
                        '$set': {
                            'Description._ru': translated_result.lower()
                        }
                    }
                )

    def get_unique_results(self, iter_object: list) -> list:
        return [unique for index, unique in enumerate(iter_object) if unique not in iter_object[:index]]

error = BulkWriteError
link = os.getenv('mongodb_uri')
mongo_search = MongoDB(link)
mongo_search.collection = 'emojis'
result_all = mongo_search.show_results()
mongo_common = MongoDB(link)

users_filter = {'name': {'$regex': '(.*expanded.*)'}}
users_dbs = mongo_common.db.list_collection_names(filter=users_filter)
users_dbs_lst = [mongo_common.show_results(collection_name=user) for user in users_dbs if user]



if __name__ == '__main__':
    mongo = MongoDB(os.getenv('mongodb_uri'))
    mongo.collection = 'emojis'
    print(mongo.search_emoji(name='grapes'))
