import os

USER = os.environ.get('dbuser')
PASSW = os.environ.get('dbpass')
CLUST = os.environ.get('cluster')
TOKEN = os.environ.get('token')

data = {
    'token': f'{TOKEN}',
    'dblink': f'mongodb+srv://{USER}:{PASSW}@{CLUST}',
    'dbname': 'tg_awkward',
}