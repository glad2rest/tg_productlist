# !/usr/bin/python

import telebot
# ↓ buttons and keyboards
from telebot.types import InlineKeyboardButton, KeyboardButton
from telebot.types import InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove, CallbackQuery
import settings
# ↓ modules to work with the db
from emojis import emojis, mongo, mongo_common, error
# ↓ bot-handlers
from text_handlers import start_command_handler, start_handler, create_list_handler
from text_handlers import change_text_handler, wfe_handler, uc_emoji_handler, name_list_save_handler
from callback_handlers import act_with_lists_callback, create_list_callback, show_lists_callback, view_list_callback
from callback_handlers import del_list_callback, save_lists_callback, change_text_callback, change_emoji_callback
from callback_handlers import cancel_callback, expand_emoji_data_callback, user_db_callback
# ↓ debug logging
from telebot import logger, logging
from datetime import datetime
import json
# ↓ Typehints
from typing import TypeAlias, TYPE_CHECKING

MAX_CALLBACK_LENGTH = 50

class KeyboardBuilder:
    def keyboard_builder(self, buttons: list, reply=0, width: int = 1, add_row: int = 0) -> InlineKeyboardMarkup:
        if reply == 1:
            keyboard = ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            keyboard.add(*buttons)
            return keyboard
        keyboard = InlineKeyboardMarkup()
        keyboard.row_width = width
        if add_row == 0:
            keyboard.add(*buttons)
        else:
            keyboard.row(*buttons)
        return keyboard
          
    def button_create(self, text, callback=None, reply=0):
        if reply == 1:
            return KeyboardButton(text=text)
        # prevent API error with BUTTON_DATA_INVALID
        if len(callback) > MAX_CALLBACK_LENGTH:
            callback = callback[:MAX_CALLBACK_LENGTH]
        return InlineKeyboardButton(text=text, callback_data=callback)


kb = KeyboardBuilder()


class FoodBot:
    sticker_id = 'CAACAgIAAxkBAAI0_mRVCmfvYvWjzFV1uqIZ4bwhtDvBAAIfAAPANk8T5Dgz94RSmZIvBA'  # wormy sticker
    msg_session_disconnect = 'Упс, похоже, сессия была прервана, пожалуйста, введите в чат /start для инициализации новой сессии.'
    
    def __init__(self, token) -> None:
        self.bot = telebot.TeleBot(token=token)
        self.session = dict()
        self.bot_id = self.bot.get_me().id
        self.exception_callback = None

        self.keyboard_start = None
        self.keyboard_create_list = None

        @self.bot.message_handler(commands=['start'])
        def start_session(message):
            self.keyboard_start = start_handler.keyboard_create(kb, mongo_common, user_id=message.from_user.id)
            self.keyboard_create_list = create_list_handler.keyboard_create(kb)

            user_id = message.from_user.id

            start_command_handler.auth(self, message)
            start_handler.message(self, message, user_id)
            self.session[user_id]['already_welcomed'] = True

        # state overseer
        @self.bot.message_handler(content_types=['text'])
        def message_handler(message):
            user_id: int = message.chat.id
            if user_id not in self.session:
                self.update_session(message=message)
                
            data = self.session[user_id]
            
            if data['current_handler'] == None:
                button_start = kb.button_create(text='/start', reply=1)
                self.keyboard_end_of_session = kb.keyboard_builder(buttons=[button_start], reply=1)
                self.bot.send_sticker(chat_id=message.from_user.id, sticker=self.sticker_id)
                self.bot.send_message(chat_id=message.chat.id, text=self.msg_session_disconnect, reply_markup=self.keyboard_end_of_session)

            elif data['current_handler'] == 'start_handler':
                start_handler.message(self, message, user_id)

            elif data['current_handler'] == 'create_list_handler':
                if len(message.text) > MAX_CALLBACK_LENGTH:
                    create_list_handler.warning(self, message, MAX_CALLBACK_LENGTH, user_id)
                else: 
                    create_list_handler.update_session_with_matched_emojis(self, message, user_id)
                    create_list_handler.message(self, message, user_id)

            elif data['current_handler'] == 'change_text_handler':
                change_text_handler.editor(self, data, message, user_id)
                
            elif data['current_handler'] == 'waiting_for_emoji_handler':
                wfe_handler.message(self, data, message, user_id)

            elif data['current_handler'] == 'user_chosing_emoji_handler':
                uc_emoji_handler.message(self, mongo_common, data, message, user_id)

            elif data['current_handler'] == 'view_not_saved_list_handler':
                del data['keyboard_view_elements']
                data['current_handler'] = 'create_list_handler'
                create_list_handler.update_session_with_matched_emojis(self, message, user_id)
                create_list_handler.message(self, message, user_id)

            elif data['current_handler'] == 'type_list_name_before_save_handler':
                name_list_save_handler.save_list(self, kb, mongo_common, message, data, user_id)
                
            print(self.session, '\n', self.session[user_id]['current_handler'], r'{handler}')

        @self.bot.callback_query_handler(func=lambda callback: True)
        def callback_handler(callback: CallbackQuery):
            user_id = callback.message.chat.id
            
            data = self.session.get(user_id, {})
            if not data:
                callback.data = ''

            try:
                if callback.data == 'create_list_call':
                    create_list_callback.message(self, kb, callback, user_id)
                
                elif callback.data == 'show_lists_call':
                    show_lists_callback.generate_response(self, kb, mongo_common, callback, user_id)

                elif callback.data == 'view_list_call':
                    view_list_callback.generate_response(self, kb, data, user_id)
                    data['current_handler'] = 'view_not_saved_list_handler'
                    self.bot.answer_callback_query(callback_query_id=callback.id)

                
                elif callback.data.startswith(str(user_id)):
                     if 'del' in callback.data:
                          act_with_lists_callback.delete_chosen_list(self, kb, callback, data, user_id)
                     else:
                          act_with_lists_callback.view_chosen_list(self, kb, callback, data, user_id)

                elif 'del_list' in self.session.get(user_id, {}):
                    del_list_callback.delete(self, kb, mongo_common, callback, data, user_id)
                
                elif callback.data == 'save_list_call':
                    save_lists_callback.generate_response(self, kb, data, user_id)
                    data['current_handler'] = 'type_list_name_before_save_handler'
                    self.bot.answer_callback_query(callback_query_id=callback.id)

                elif callback.data == 'cancel_call':
                    cancel_callback.cancel(self, callback, data, user_id)
                    self.bot.answer_callback_query(callback_query_id=callback.id)

                elif callback.data == 'cancel_save':
                    cancel_callback.cancel_save(self, data, user_id)
                    self.bot.answer_callback_query(callback_query_id=callback.id)

                elif callback.data in ['yes_expand', 'no_expand']:
                    expand_emoji_data_callback.generate(self, kb, mongo_common, callback, data, user_id)

                elif callback.data == 'change_text_call':
                    change_text_callback.change(self, kb, data, user_id)

                elif callback.data == 'change_emoji':
                    change_emoji_callback.switch_handler(self, kb, data, user_id)
                    self.bot.answer_callback_query(callback.id)

                elif callback.data == 'add_emo':
                    message = self.bot.send_message(chat_id=user_id, text='Отправьте в чат эмоцию:')
                    self.session[user_id]['message_id'] = [data['message_id'], message.message_id]
                    self.session[user_id]['current_handler'] = 'user_chosing_emoji_handler'
                
                elif '_done' in callback.data:
                    view_list_callback.checkboxing(self, callback, data, user_id)

                elif callback.data.startswith('emo'):  # callbacks generated in the create_list_handler.py
                    create_list_callback.change_emoji(self, callback, data)

                elif callback.data == 'erase_udb_call':
                    user_db_callback.confirm_message(self, kb, user_id)
                
                elif callback.data == f'erase_{user_id}_db':
                    user_db_callback.erase(self, kb, mongo_common, callback, user_id)

                elif callback.data == 'back_to_the_main_call':
                    create_list_callback.main_menu(self, callback, user_id)
                    self.bot.answer_callback_query(callback.id)

            except Exception as ex:
                import traceback
                print(f'\nEXCEPTION raises here ↓\n')
                user = callback.message.from_user.first_name
                print(f'{ex, user}. [exception_callback]')
                traceback.print_exc()
                def drop_session():
                    button_start = kb.button_create(text='/start', reply=1)
                    self.keyboard_end_of_session = kb.keyboard_builder(buttons=[button_start], reply=1)
                    self.bot.send_sticker(chat_id=user_id, sticker=self.sticker_id)
                    self.bot.send_message(chat_id=user_id, text=self.msg_session_disconnect, reply_markup=self.keyboard_end_of_session)
                drop_session()
                self.log_show(message=callback.message)

        @self.bot.message_handler(content_types=['audio', 'photo', 'video', 'document', 'sticker'])
        def trash_cleaner(message):
            sticker = message.sticker is not None
            document = message.document is not None
            video = message.video is not None

            if sticker or document or video:
                sticker_id = message.sticker.file_id
                print(type(sticker_id))
                print(f'{message.from_user.id, message.from_user.first_name} sends the sticker with ID = {sticker_id}')
                self.bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)

    def create_list(self, message, reset: bool = False) -> None:
        food_list = self.session[message.chat.id]['food_list']
        
        match_list = list()
        emoji = list()
        message_list = message.text.lower().split()

        if reset:
            food_list.clear()
            self.session[message.chat.id]['counter'] = 0
            self.session[message.chat.id]['message_id'] = ''

        elif not reset:
            match_list = emojis.get_matches(name=message_list, user=str(message.from_user.id))

            match message.text:
                case message.text if message.from_user.id != self.bot_id:
                    self.session[message.chat.id]['counter'] += 1
                    counter = self.session[message.chat.id]['counter']
                    if match_list != [[]]:
                        user_list = match_list[:-1]
                        common_list = match_list[-1]
                        if any('user' in str(elem) for elem in user_list):
                            user_list.pop()
                        if common_list:
                            common_list = emojis.generate_emoji(common_list)
                        emoji = user_list + common_list
                        if emoji:
                            response = f'{counter}. {emoji[0]} {message.text}'  # optimize me
                        else:
                            response = f'{counter}. {message.text}'    
                    else:
                        response = f'{counter}. {message.text}'
                    
                    food_list = food_list.append(f'{response}')

        return emoji if emoji else None
          
    def update_session(self, message):
        user_id = message.chat.id
        first_name = message.from_user.first_name
        if user_id not in self.session:
            updates = {
                user_id: {
                    'counter': 0,
                    'first_name': first_name,
                    'current_handler': None,
                    'message_id': None,
                    'food_list': list(
                        # the place to append products
                    )
                }
            }
            self.session.update(updates)
        return user_id
    
    def log_show(self, message):
        logger = telebot.logger
        telebot.logger.setLevel(logging.CRITICAL)
        print(f'{str(datetime.now()):.^100}')
        if message.from_user.is_bot == True:
            text = f'The user find a bug: ({message.chat.id}, {message.chat.first_name}).'
        else:
            text = message.text
        print(f'Message from id = {str(message.from_user.id)}, name = {str(message.from_user.first_name)}\
              \n{text} [message_block]\
              \n{logger} [logger_error]\
              \n{logger.debug(msg=message)} [logger_debug]')
        print(f'{error.details} [error_details]')
        print(json.dumps(self.session, indent=4, sort_keys=True), '[sessionblock]')
        print(f'{str("."):.^100}')

    def main(self):
        self.bot.polling(none_stop=True, interval=0)


if __name__ == '__main__':
    bot = FoodBot(token=settings.data['token'])
    bot.main()