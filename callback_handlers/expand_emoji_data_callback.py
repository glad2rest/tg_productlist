import time
from text_handlers import start_handler

def generate(food, kb, mongo_common, callback, data, user_id):
    expanded_db = food.session[user_id]['mark_expand_db']
    text = 'Список сохранен успешно.'
    message_id = data['message_id']

    if 'yes' in callback.data:
        collection_name = f'{user_id}_expanded_data'
        # typing-trick
        message = food.bot.edit_message_text(chat_id=user_id, message_id=message_id, text='Секундочку, сохраняю.')
        food.bot.send_chat_action(chat_id=callback.message.chat.id, action='typing', message_thread_id=callback.message.chat.id, timeout=2)

        mongo_common.save_users_collection_docs(collection_name=collection_name, data=expanded_db, expand=True)
        mongo_common.update_users_collection(collection_name=collection_name)
        text = f'Список успешно "{data["list_name"]}" сохранен и данные о новых эмоциях занесены в бд'

    else:
        message = food.bot.edit_message_text(chat_id=user_id, message_id=message_id, text='Секундочку, сохраняю')
    food.bot.answer_callback_query(callback.id)

    food.bot.send_chat_action(chat_id=callback.message.chat.id, action='typing', message_thread_id=callback.message.chat.id, timeout=2)
    mongo_common.save_users_collection_docs(collection_name=str(user_id), data=data)
    food.bot.answer_callback_query(callback.id)  

    food.bot.send_message(callback.message.chat.id, text=text)
    time.sleep(1)

    # erase 'food_list'
    food.create_list(callback.message, reset=True)

    if food.session[user_id].get('mark_expand_db'):
        del food.session[user_id]['mark_expand_db']
    food.bot.delete_message(chat_id=user_id, message_id=message.message_id)

    food.bot.send_message(chat_id=callback.message.chat.id, text='Выберите действие.', reply_markup=food.keyboard_start)
    food.session[user_id]['current_handler'] = 'start_handler'