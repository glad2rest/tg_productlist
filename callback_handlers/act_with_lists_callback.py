def delete_chosen_list(food, kb, callback, data, user_id):
    """
    Builds a keyboard with two buttons ['yes', 'no'] and sends the user a question with the choice of his action.
    receives: FoodBot() instance, KeyboardBuilder() instance, CallbackQuery() instance, data: dict(), iser_id: int
    returns: None
    """

    # callback.data value is like 'UserID_del_ListName'
    # maxsplit=2 implemented to prevent errors can be caused by using underscores in the collection name
    list_name = callback.data.split('_', maxsplit=2)[2]

    def nested_keyboard_with_choice():
        buttons_list = list()
        button_yes = kb.button_create(text='Удалить', callback=f'{list_name}_yes')
        button_no = kb.button_create(text='Отмена', callback=f'show_lists_call')
        buttons_list += button_yes, button_no
        keyboard_list = kb.keyboard_builder(buttons=buttons_list, width=2)
        return keyboard_list
    
    message = food.bot.send_message(
        chat_id=callback.message.chat.id, 
        text=f'\N{thinking face} Уверены, что хотите удалить: "{list_name}"?', 
        reply_markup=nested_keyboard_with_choice()
    )
    food.session[user_id]['message_id'] = message.message_id

    # the data['saved_lists'] structure is {'list_name': ['grapes', 'milk', etc.]}
    # so we iterate over the keys of the dict to find an exact match of the list_name
    # because the list_name is a splitted "callback" generated in show_lists_callback.keyboard_create_or_update(*args)
    # so callback is an truncated echo of the list_name due to its max length limit
    #  - max_callback_length=60
    #  - callback structure is 'UserID_del_ListName' where UserID is an integer with length from 10 to 12
    #  - after the split('_')[2] we got 'ListName' element - almost pure list_name
    #  - if the ListName is over 50 we'll never guess its exact length
    #  - so the trick is to find an almost exact match, e.g. 'abcd' in 'abcdefgh'
    for long_name_searcher in data['saved_lists']:
        if list_name in long_name_searcher:
            food.session[user_id]['del_list'] = long_name_searcher

    food.bot.answer_callback_query(callback.id)

def view_chosen_list(food, kb, callback, data, user_id):
    chosen_list = list()
    # выбираем список для отображения по-элементно и в конце прикрепляем кнопки создать-посмотреть-очистить
    for key, value in food.session[user_id]['saved_lists'].items():
        # maxsplit=1 because query == list_name and list_name can be like "$userid_name_of_the_database"
        #                                                               several splits prevent
        chosen_list_name = callback.data.split('_', maxsplit=1)[1]
        # query = callback if long callback[:60] when button created
        # key = name of the db writing in the session
        # so equal is callback == key[:60]
        # IN coz chosen_list_name is a splitted callback if list_name IS VERY LONG
        # so to prevent error USE x in y
        if chosen_list_name in key:
            chosen_list = value
            break

    food.bot.answer_callback_query(callback.id)
    if chosen_list:
        buttons = list()
        for element in chosen_list:
            buttons.append(kb.button_create(text=element, callback=f'{element}_done'))
        keyboard = kb.keyboard_builder(buttons)
        data['keyboard_view_elements'] = keyboard
        keyboard.add(kb.button_create(text='Назад', callback='back_to_the_main_call'))
        msg = food.bot.send_message(chat_id=user_id, text='Список:', reply_markup=keyboard)
        data['message_id'] = msg.message_id