def message(food, kb, callback, user_id):
    food.session[user_id]['current_handler'] = 'create_list_handler'

    button_back = kb.button_create(text='back', callback='back_to_the_main_call')
    keyboard = kb.keyboard_builder([button_back])

    text = 'Вводите продукты. После завершения нажмите сохранить или показать.'
    food.bot.send_message(callback.message.chat.id, text=text, reply_markup=keyboard)
    food.bot.answer_callback_query(callback.id)


def change_emoji(food, callback, data):
    matched_emojis = data['matched_emojis']
    food_list = data['food_list']

    change_emoji: str = matched_emojis[callback.data]   # the value of a dict by callback-key, e.g. '🐢' (the emoji matched to the button we push)
    food_last_record: list = food_list[-1].split()      # e.g. ['1.', '🦐', 'my_elem']
    food_last_record[1]: str = change_emoji             # ['1.', '🐢', 'my_elem']
    food_list[-1]: str = ' '.join(food_last_record)     # '1. 🐢 my_elem'

    message_id = data['message_id']
    text = f'Добавлен: {food_list[-1]}'

    food.bot.edit_message_text(
        chat_id=callback.message.chat.id, 
        message_id=message_id, 
        text=text, 
        reply_markup=food.keyboard_create_list
    )
    
    if data['current_handler'] == 'waiting_for_emoji_handler':
        data['current_handler'] = 'create_list_handler'

def main_menu(food, callback, user_id):
    food.create_list(callback.message, reset=True)

    if food.session[user_id].get('mark_expand_db'):
        del food.session[user_id]['mark_expand_db']

    food.bot.send_message(
        chat_id=callback.message.chat.id, 
        text='Выберите действие:', 
        reply_markup=food.keyboard_start
    )
    food.session[user_id]['current_handler'] = 'start_handler'