from callback_handlers.show_lists_callback import keyboard_create_or_update

def delete(food, kb, mongo_common, callback, data, user_id):
    # callback.data == food.session[user_id]["del_list"], e.g. 'mylist1'
    list_name = data['del_list']
    extra_msg_id = data['extra_message_id']

    mongo_common.del_document(collection_name=str(user_id), key='list_name', value=list_name)
    food.bot.answer_callback_query(callback_query_id=callback.id)
    del data['del_list']
    del data['extra_message_id']
    
    food.bot.delete_message(chat_id=user_id, message_id=data['message_id'])

    mongo_results = mongo_common.show_results(collection_name=str(user_id))
    keyboard_list = keyboard_create_or_update(kb, mongo_results, user_id)
    extra_msg_id = food.bot.edit_message_text(
        chat_id=callback.message.chat.id,
        message_id=extra_msg_id,
        text=f'Список "{list_name}" успешно удалён.\nСохранённых списков: {len(mongo_results)}', 
        reply_markup=keyboard_list,
    )
    food.bot.answer_callback_query(callback_query_id=callback.id)
    data['extra_message_id'] = extra_msg_id.message_id