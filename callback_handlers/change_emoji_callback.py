import re

def keyboard_create(food, kb, user_id):
    buttons = list()
    matches = food.session[user_id]['matched_emojis']

    if matches:
        for call, emoji in matches.items():
            buttons.append(kb.button_create(text=emoji, callback=call))
            buttons.append(kb.button_create(text='добавить', callback='add_emo'))

    buttons.append(kb.button_create(text='Назад.', callback='cancel_call'))
    keyboard_list = kb.keyboard_builder(buttons, width=6)
    return keyboard_list

def switch_handler(food, kb, data, user_id):
    matches = food.session[user_id]['matched_emojis']
    keyboard = keyboard_create(food, kb, user_id)

    if matches:
        text = 'Выберите, пожалуйста, эмоцию из списка или нажмите "+" для добавления своей эмоции к данному элементу:'
        food.session[user_id]['current_handler'] = 'waiting_for_emoji_handler'
    else:
        last_elem_in_list = food.session[user_id]['food_list'][-1].split()      # ['1.', 'my_elem'] or ['1.', '🐢', 'my_elem']
        pattern = r'\w+|\s'                                                     # letters, numbers and spaces

        if re.search(pattern, last_elem_in_list[1]):                            # if 1st element is non-emoji but pure text:
            last_elem_in_list = ' '.join(last_elem_in_list[1:])                 # 'my_elem'

        else:                                                                   # otherwise combine the list from 2nd element:
            last_elem_in_list = ' '.join(last_elem_in_list[2:])                 # 'my_elem'

        text = f'Эмоций по слову "*{last_elem_in_list}*" не найдено.\
             \nВы можете отправить в чат свою эмоцию и я её запомню в следующий раз.'
        
        pattern = r'[!|.|#]'
        text = re.sub(pattern, r'\\\g<0>', text)
        food.session[user_id]['current_handler'] = 'user_chosing_emoji_handler'

    message_id = data['message_id']

    last_message = food.bot.edit_message_text(chat_id=user_id, message_id=message_id, text=text, reply_markup=keyboard, parse_mode='MarkdownV2')
    food.session[user_id]['message_id'] = last_message.message_id


# def change(food, kb, data, user_id):
#     matches = food.session[user_id]['matched_emojis']
#     buttons = list()

#     if matches:
#         for call, emoji in matches.items():
#             buttons.append(kb.button_create(text=emoji, callback=call))
#         buttons.append(kb.button_create(text='+', callback='add_emo'))
#         text = 'Выберите, пожалуйста, эмоцию из списка или выберите + для добавления кастомной эмоции в вашу базу данных:'
#         food.session[user_id]['current_handler'] = 'waiting_for_emoji_handler' #########################
#     else:
#         last_elem_in_list = food.session[user_id]['food_list'][-1].split()
#         # если соответствия не найдено, то last_elem_in_list == '1. food'
#         if last_elem_in_list[1].isalpha() or last_elem_in_list[1].isdigit():
#             last_elem_in_list = ' '.join(last_elem_in_list[1:])
#         # если пользователь решил поменять значение эмоции несколько раз, 
#         # то тогда last_elem_in_list == '1. 🫖 food'
#         else:
#             last_elem_in_list = ' '.join(last_elem_in_list[2:])
#         text = f'Эмоций по слову {last_elem_in_list if last_elem_in_list else None} не найдено.\
#              \nВы можете отправить в чат свою эмоцию и я её запомню в следующий раз.'
#         food.session[user_id]['current_handler'] = 'user_chosing_emoji_handler'

#     buttons.append(kb.button_create(text='back', callback='cancel_call'))
#     keyboard_list = kb.keyboard_builder(buttons, width=6)

#     message_id = data['message_id']

#     last_message = food.bot.edit_message_text(chat_id=user_id, message_id=message_id, text=text, reply_markup=keyboard_list)
#     food.session[user_id]['message_id'] = last_message.message_id