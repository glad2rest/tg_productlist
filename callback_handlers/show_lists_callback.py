SAVED_LISTS = dict()

def keyboard_create_or_update(kb, mongo_results, user_id):
    buttons_list = list()
    for number, food_list in enumerate(mongo_results, start=1):  # "mongo_results" returns a list of all documents from specified collection
        list_name = food_list['list_name']
        collection_name = str(user_id)
        # generate all saved user's lists in the button's form
        buttons_list.append(
            kb.button_create(
                # depends on user's choice before save: name can be 'text' or datetime like ['1. text|03.05.2023']
                text=fr'{number}. {list_name}', 
                callback=f'{collection_name}_{list_name}'
            )
        )
        # buttons with delete option
        buttons_list.append(
            kb.button_create(
                text=f'\N{wastebasket}', 
                callback=f'{collection_name}_del_{list_name}'
            )
        )


    buttons_list.append(kb.button_create(text='назад', callback='back_to_the_main_call'))
    keyboard_list = kb.keyboard_builder(buttons=buttons_list, width=2, add_row=0)
    return keyboard_list

def generate_response(food, kb, mongo_common, callback, user_id):
    food.session[user_id]['current_handler'] = 'start_handler'
    wait_message = food.bot.send_message(chat_id=user_id, text='Секундочку, проверяю.', timeout=1)
    food.bot.send_chat_action(chat_id=user_id, action='typing', timeout=1)
    
    collection_name=str(user_id)
    if mongo_common.db_exists(collection_name=collection_name):
        food.bot.answer_callback_query(callback.id)
        mongo_results = mongo_common.show_results(collection_name=collection_name, key='time', value={'$exists': True})
        list_count = len(mongo_results)

        for food_list in mongo_results:
        #   {'04.05.23': ['value_one', 'value_two']}
            list_name = food_list['list_name']
            SAVED_LISTS.update({list_name: food_list['food_list']})
        
        keyboard_list = keyboard_create_or_update(kb, mongo_results, user_id)
        food.session[user_id]['keyboard_list'] = keyboard_list

        text = f'Сохранённых списков: {list_count}'
        count = mongo_common.count_documents(collection_name=collection_name, query='all')
        if not count:
            keyboard_list = food.keyboard_start

        wait_message_id: int = wait_message.message_id
        get_extra_id: int = food.session[user_id].get('extra_message_id', 0)
        extra_message_id: int = 0
        
        if callback.message.message_id == get_extra_id:
            food.bot.delete_message(chat_id=user_id, message_id=wait_message_id)
        else:
            extra_message_id = food.bot.edit_message_text(
                chat_id=user_id, 
                message_id=wait_message_id, 
                text=text, 
                reply_markup=keyboard_list
            )

        food.session[user_id]['saved_lists'] = SAVED_LISTS  # update session

        if extra_message_id:
            food.session[user_id]['extra_message_id'] = extra_message_id.message_id
            
    else:
        # condition when user hasn't any collection
        text = f'У вас нет сохранённых списков, попробуйте создать первый.'
        food.bot.answer_callback_query(callback.id)
        food.bot.edit_message_text(
            chat_id=callback.message.chat.id, 
            message_id=wait_message.message_id, 
            text=text,
        )
