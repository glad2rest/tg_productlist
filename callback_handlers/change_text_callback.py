import re

def change(food, kb, data, user_id):
    data['original_text']: str = data['food_list'][-1]  # '1. my_elem' or '1. 🐢 my_elem'
    current_row: list = data['food_list'][-1].split()   # ['1.', 'my_elem'] or ['1.', '🐢', 'my_elem']
    prefix: str = None
    pattern = r'\w+|\s'                                 # all letters, numbers and spaces
    if re.search(pattern, current_row[1]):              # if no emoji matches current_row:
        prefix: str = ' '.join(current_row[:1])         # '1.'
        current_row: str = ' '.join(current_row[1:])    # 'my_elem'
    else:                                               # otherwise:
        prefix: str = ' '.join(current_row[:2])         # '1. 🐢'
        current_row: str = ' '.join(current_row[2:])    # 'my_elem'
        
    data['changing_text_prefix'] = prefix
    back_button = kb.keyboard_builder(
        [
            kb.button_create('back', 'cancel_call')
        ]
    )
    text = f'Введите текст для изменения записи{" {}".format(current_row) if current_row else ""}.'
    message = food.bot.edit_message_text(chat_id=user_id, message_id=data['message_id'], text=text, reply_markup=back_button)
    data['message_id'] = message.message_id
    data['current_handler'] = 'change_text_handler'