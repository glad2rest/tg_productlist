from text_handlers import start_handler
import time

def confirm_message(food, kb, user_id):
    def keyboard(kb, user_id):
        button_yes = kb.button_create(text='Удалить', callback=f'erase_{user_id}_db')
        button_no = kb.button_create(text='Отмена', callback='back_to_the_main_call')
        buttons = [button_yes, button_no]
        keyboard = kb.keyboard_builder(buttons=buttons, width=2)
        return keyboard
    
    text = 'Вы уверены? Ваша баша база данных с эмоциями будет очищена.'
    food.bot.send_message(
        chat_id=user_id, 
        text=text, 
        reply_markup=keyboard(kb, user_id))
    
def erase(food, kb, mongo_common, callback, user_id):
    collection_name = f'{user_id}_expanded_data'
    try:
        mongo_common.truncate_collection(collection_name=collection_name)
    finally:
        food.bot.answer_callback_query(callback.id)
        food.keyboard_start = start_handler.keyboard_create(kb, mongo_common, user_id)
        food.bot.send_message(
            chat_id=user_id, 
            text='Ваша база эмоций успешно удалена, пожалуйста, выберите действие:', 
            reply_markup=food.keyboard_start
        )
    time.sleep(1)