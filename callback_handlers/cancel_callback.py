def cancel(food, callback, data, user_id):
    condition_handler = 'view_not_saved_list_handler'
    current_handler = data['current_handler']

    if data.get('keyboard_view_elements') and current_handler != condition_handler:
        # if 'cancel' pushed in the view-function {callback.data: 'view_list_call'}
        msg = food.bot.edit_message_text(
            chat_id=user_id, 
            message_id=data['message_id'], 
            text=data['food_list'][-1], 
            reply_markup=food.keyboard_create_list
        )
        data['message_id'] = msg.message_id
        
    elif current_handler == condition_handler:
        food.bot.delete_message(chat_id=user_id, message_id=callback.message.message_id)

    else:
        # 'cancel' button in the edit-text/edit-emoji functions
        food_list = food.session[user_id]['food_list']
        text = f'Добавлен: {food_list[-1]}'
        msg = food.bot.edit_message_text(
            chat_id=user_id, 
            message_id=data['message_id'], 
            text=text, 
            reply_markup=food.keyboard_create_list
        )
        data['message_id'] = msg.message_id
        

    data['current_handler'] = 'create_list_handler'

def cancel_save(food, data, user_id):
    food.bot.delete_message(chat_id=user_id, message_id=data['save_message_id'])
    data['current_handler'] = 'create_list_handler'
