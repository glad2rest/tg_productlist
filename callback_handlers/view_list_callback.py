# view option for not saved list
def generate_response(food, kb, data, user_id):
    # iterates through the "food-list" sequence
    # sends replies where each reply is the button

    if data.get('food_list'):
        buttons = list()
        for element in data['food_list']:
            buttons.append(kb.button_create(text=element, callback=f'{element}_done'))
        keyboard = kb.keyboard_builder(buttons)
        data['keyboard_view_elements'] = keyboard
        keyboard.add(kb.button_create(text='back', callback='cancel_call'))
        food.bot.send_message(chat_id=user_id, text='Список:', reply_markup=keyboard)

        ##############################################
        #                deprecated                  #
        #  not_last_message = len(data['food_list']) #
        #  for element in data['food_list']:         #
        #      not_last_message -= 1                 #
        #      if not_last_message:                  #
        #        markup = None                       #
        #      else:                                 #
        #        markup = food.keyboard_create_list  #
        #                                            #
        # last_message = food.bot.send_message(      #
        #  chat_id=user_id,                          #
        #  text=element,                             #
        #  reply_markup=markup                       #
        # )                                          #
        ##############################################
        
    else:
        food.bot.send_message(chat_id=user_id, text='Похоже, что Ваш список пуст.')
    
def checkboxing(food, callback, data, user_id):
    food.bot.answer_callback_query(callback_query_id=callback.id)
    text = callback.data.split('_')[0]  # pure callback equal to the list's name
    keyboard = data['keyboard_view_elements']  # get the keyboard object stored in the session
    check = '\N{check mark}'

    # updates the keyboard depending on buttons we pushed
    for buttons in keyboard.keyboard:                # iterate through the keyboard
        for button in buttons:                       # to get each button
            if text in button.text:                  # and button text
                if check in button.text: 
                    changes = button.text.split()    # if button pushed
                    changes.pop() 
                    button.text = ' '.join(changes)  # remove check-mark symbol
                else:                                # otherwise
                    button.text = f'{text} {check}'  # put it at the end of the button name

    # edits message with the new keyboard
    food.bot.edit_message_reply_markup(
        chat_id=user_id, 
        message_id=callback.message.message_id, 
        reply_markup=keyboard
    )
