def generate_response(food, kb, data, user_id):
    text = 'Введите имя списка перед сохранением или введите любую цифру для сохранения списка с именем текущей даты.'

    button_cancel = kb.button_create(text='отмена', callback='cancel_save')
    keyboard = kb.keyboard_builder([button_cancel])
    
    message = food.bot.send_message(chat_id=user_id, text=text, reply_markup=keyboard)
    data['save_message_id'] = message.message_id
    